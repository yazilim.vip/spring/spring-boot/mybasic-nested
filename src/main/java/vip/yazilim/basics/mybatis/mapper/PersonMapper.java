package vip.yazilim.basics.mybatis.mapper;

import org.apache.ibatis.annotations.*;
import vip.yazilim.basics.mybatis.model.Location;
import vip.yazilim.basics.mybatis.model.Person;

import java.util.List;

@Mapper
public interface PersonMapper {

    @Insert("INSERT INTO Location (City, Country) values (#{city},#{country})")
    @Options(useGeneratedKeys = true, keyProperty = "locationId")
    int insertLocation(Location location);

    @Insert("INSERT INTO Person (Name, LocationId, BirthDate) values (#{person.name},#{locationId}, #{person.birthDate})")
    int insetPerson(@Param("person")Person person, @Param("locationId") int locationId);

    @Select("SELECT * FROM Person")
    List<Person> findAllPerson();

    @Select("SELECT * FROM Location where locationId=#{locationId}")
    Location findAllPerson(int locationId);
}
