package vip.yazilim.basics.mybatis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vip.yazilim.basics.mybatis.mapper.PersonMapper;
import vip.yazilim.basics.mybatis.model.Person;

import java.sql.SQLException;
import java.util.List;

@Service
public class PersonService {
    @Autowired
    private PersonMapper personMapper;
    Logger logger = LoggerFactory.getLogger(PersonService.class);

    public int insertPerson(Person person) {
        try {
            personMapper.insertLocation(person.getLocation());
            personMapper.insetPerson(person, person.getLocation().getLocationId());
            return 1;
        }catch (Exception e){
            logger.error(e.getMessage());
            return 0;
        }
    }

    public List<Person> finfAllPerson() {
        try {
            personMapper.findAllPerson();
        }catch (Exception e){
            logger.error(e.getMessage());
            return null;
        }
    }
}
