package vip.yazilim.basics.mybatis.model;

public class Location {
    private int locationId;
    private String City;
    private String country;

    public Location(int locationId, String city, String country) {
        this.locationId = locationId;
        City = city;
        this.country = country;
    }

    public Location(String city, String country) {
        City = city;
        this.country = country;
    }

    public Location(int locationId) {
        this.locationId = locationId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Location{" +
                "locationId=" + locationId +
                ", City='" + City + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
