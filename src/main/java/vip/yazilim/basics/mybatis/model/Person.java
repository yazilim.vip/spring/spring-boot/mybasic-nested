package vip.yazilim.basics.mybatis.model;

import java.util.Date;

public class Person {
    private int PersonId;
    private String name;
    private Location location;
    private Date birthDate;

    public Person(String name, Location location, Date birthDate) {
        this.name = name;
        this.location = location;
        this.birthDate = birthDate;
    }

    public Person(int personId, String name, Location location, Date birthDate) {
        PersonId = personId;
        this.name = name;
        this.location = location;
        this.birthDate = birthDate;
    }

    public Person() {
    }

    public int getPersonId() {
        return PersonId;
    }

    public void setPersonId(int personId) {
        PersonId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Person{" +
                "PersonId=" + PersonId +
                ", name='" + name + '\'' +
                ", location=" + location +
                ", birthDate=" + birthDate +
                '}';
    }
}
