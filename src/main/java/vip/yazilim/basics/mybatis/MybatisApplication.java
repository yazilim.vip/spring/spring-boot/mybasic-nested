package vip.yazilim.basics.mybatis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import vip.yazilim.basics.mybatis.model.Location;
import vip.yazilim.basics.mybatis.model.Person;
import vip.yazilim.basics.mybatis.service.PersonService;

import java.util.Date;

@SpringBootApplication
public class MybatisApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(MybatisApplication.class, args);
		PersonService personService = ctx.getBean(PersonService.class);

		Location location = new Location("Adana","Türkiye");
		Person person = new Person("anıl", location,new Date());
		personService.insertPerson(person);



	}
}
