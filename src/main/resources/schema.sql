create table Location(
  LocationId integer not null AUTO_INCREMENT,
  City varchar(50),
  Country varchar(50),
  primary key(LocationId)
);

create table Person(
  PersonId integer not null AUTO_INCREMENT,
  Name varchar(50),
  LocationId integer,
  BirthDate timestamp,
  primary key(PersonId),
  foreign key(LocationId) references Location (LocationId)
);

